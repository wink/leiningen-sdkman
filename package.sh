#!/bin/bash

if [ "$1" = "" -o "$2" = "" ]; then
  echo "Usage: $0 VERSION BUILD_DIR"
  exit
fi

VERSION="$1"
BUILD_DIR="$2"
SUB_DIR="leiningen-${VERSION}"
OUT_DIR="out"
ODIR=$(dirname $(readlink -f $0))

test -d "$BUILD_DIR" && {
  echo "The build dir ${BUILD_DIR} exists, choose a different dir."
  exit 1
}

echo "Packaging Leiningen ${VERSION} for SDKMAN in directory '${BUILD_DIR}' ..."
echo

mkdir -p "$BUILD_DIR"
cd "$BUILD_DIR"

test -d "$SUB_DIR" || mkdir -p "$SUB_DIR"
cd "$SUB_DIR"

mkdir lib
cd lib

echo "Downloading: leiningen-${VERSION} jar"
wget -q "https://github.com/technomancy/leiningen/releases/download/${VERSION}/leiningen-${VERSION}-standalone.jar" -O "leiningen-${VERSION}-standalone.jar"

cd ..
mkdir bin
cd bin

echo "Downloading: lein.bat"
wget -q "https://raw.githubusercontent.com/technomancy/leiningen/${VERSION}/bin/lein.bat" -O "lein.bat"
chmod +x lein.bat

echo "Copying: lein-pkg"
wget -q "https://raw.githubusercontent.com/technomancy/leiningen/${VERSION}/bin/lein-pkg" -O "lein-pkg"
cp "${ODIR}/src/lein-sdkman" lein
echo "------------------------------------------"
colordiff -u lein-pkg lein
echo "------------------------------------------"
echo "this is a diff between lein-pkg and lein-sdkman, update manually"
echo "------------------------------------------"
chmod +x lein
chmod +x lein-pkg

cd ../..
test -d "$OUT_DIR" || mkdir -p "$OUT_DIR"

FILE_NAME="${OUT_DIR}/${SUB_DIR}.zip"

echo "Now cd to the build-dir (it has a dir called 'leiningen-xx') and run this:\n"
echo zip -r "$FILE_NAME" "$SUB_DIR"

echo
#echo "Leiningen ${VERSION} was packaged to ${FILE_NAME}"

