#!/bin/bash

if [ "$1" = "" ]; then
  echo "Usage: $0 VERSION"
  exit
fi

VERSION="$1"
#URL="https://github.com/leiningen/leiningen-sdkman/releases/download/${VERSION}/leiningen-${VERSION}.zip"
FILE=$(mktemp)
URL="https://codeberg.org/api/v1/repos/wink/leiningen-sdkman/releases/tags/${VERSION}"
JSON=$(curl $URL > $FILE 2>/dev/null)
URL2=$(cat "$FILE" | jq ".assets[0].browser_download_url" | sed 's/"//g')
'rm' -i $FILE

curl -X POST \
-H "Consumer-Key: ${CONSUMER_KEY}" \
-H "Consumer-Token: ${CONSUMER_TOKEN}" \
-H "Content-Type: application/json" \
-H "Accept: application/json" \
-d "{\"candidate\": \"leiningen\", \"version\": \"${VERSION}\", \"url\": \"${URL2}\"}" \
https://vendors.sdkman.io/release

echo
