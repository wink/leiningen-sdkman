# Changelog for leiningen-sdkman

For changes in Leiningen proper, please see https://codeberg.org/leiningen/leiningen/src/branch/main/NEWS.md

  * 2.11.2
    * Made the release process a bit more manual, but check `lein-pkg` upstream
  * 2.11.1-1
    * Revert the packaging change, combo of user error and horrible UI
  * 2.11.1
    * Apparently Codeberg changed their api response
    * lein-sdkman was removed from upstream for 2.9.9, so maybe `src/lein-sdkman` needs to be synced differently
